﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.Events;
using NUnit.Framework;


namespace AtlassianTestPO
{
    public class Test_CreateNewIssue
    {
        private static IWebDriver driver = null;
        
        public static void Main(string[] args)
        {           
            driver = new FirefoxDriver();

            driver.Navigate().GoToUrl("https://jira.atlassian.com/browse/TST");

            try
            {                               
                
                driver.FindElement(By.Id("create_link")).Click();

                driver.FindElement(By.Id("project-field")).SendKeys("A Test Project (TST)");

                driver.FindElement(By.Id("issuetype-field")).SendKeys("Task");

                driver.FindElement(By.Id("summary")).SendKeys("creating a new issue");

                driver.FindElement(By.Id("create-issue-submit")).Click();

                driver.FindElement(By.PartialLinkText("creating a new issue")).Click();

            }

            catch (NoSuchElementException e)
            {
                e.Message.ToString();
            }            

                driver.Quit();                          
       
          
        }
    }
}
